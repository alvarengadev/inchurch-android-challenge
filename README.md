# inChurch Recruitment Process 2020 - Android Developer

## Arquitetura ✏️

- MVP (Model-View-Presenter)

Decidi utilizar MVP, pois é uma arquitetura na qual estou aprendendo e porque apresenta o conceito de ter uma camada específica para realizar as aprensentações de dados (Presenter) e realizar a comunicação entre as camadas Model e View!

## Bibliotecas usadas ✏️

- Retrofit
- Moshi
- Picasso
- Room

Utilizei Retrofit e Moshi para realizar as chamadas e Picasso para recuperar as imagens a partir de suas URLs, pois é uma das bibliotecas com amplo uso na comunidade. Já a Room é uma lib que permitindo realizar a persistência de dados com o SQLite utilizando o conceito ORM (Object-relational mapping). 

**Lucas Alvarenga**
